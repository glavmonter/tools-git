/*
 ============================================================================
 Name        : CalcStepper.c
 Author      : 
 Version     :
 Copyright   : Your copyright notice
 Description : Hello World in C, Ansi-style
 ============================================================================
 */

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <math.h>
#include <pthread.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <ctype.h>

#define NUM_THREADS	    20	

double freq(double speed) {
double K = 40.0*60.0*60.0*80.0/(21.0*3.0*4.0*4.0*4.0);
double D = 1.0/2.0;

    return K*speed*200.0/(3600.0*D);
}


double real_freq(uint32_t prescaller, uint32_t period) {
    return 168e6/((prescaller + 1)*(period + 1));
}


double find_minimum_freq(double need_freq, uint32_t *p_presc, uint32_t *p_period, FILE *f) {
double old_delta = real_freq(0, 0);

    for (uint32_t presc = 0; presc <= 65535; presc++) {
        switch (presc) {
        case 6550*0:
            fprintf(f, "9 ");
            fflush(f);
            break;
        case 6550*1:
        	fprintf(f, "8 ");
        	fflush(f);
            break;
        case 6550*2:
        	fprintf(f, "7 ");
        	fflush(f);
            break;
        case 6550*3:
        	fprintf(f, "6 ");
        	fflush(f);
            break;
        case 6550*4:
        	fprintf(f, "5 ");
        	fflush(f);
            break;
        case 6550*5:
        	fprintf(f, "4 ");
        	fflush(f);
            break;
        case 6550*6:
        	fprintf(f, "3 ");
        	fflush(f);
            break;
        case 6550*7:
        	fprintf(f, "2 ");
        	fflush(f);
            break;
        case 6550*8:
        	fprintf(f, "1 ");
        	fflush(f);
            break;
        case 6550*9:
        	fprintf(f, "0 ");
        	fflush(f);
            break;
        case 6550*10:
        	fprintf(f, "#\n");
        	fflush(f);
            break;
        default:
            break;
        }

        for (uint32_t period = 0; period <= 65535; period++) {
            double ff = real_freq(presc, period);
            double delta = fabs(need_freq - ff);

            if (delta < old_delta) {
                old_delta = delta;

                *p_presc = presc;
                *p_period = period;
            }
        }
    }

    return old_delta;
}

#define handle_error_en(en, msg) \
        do { errno = en; perror(msg); exit(EXIT_FAILURE); } while (0)

#define handle_error(msg) \
        do { perror(msg); exit(EXIT_FAILURE); } while (0)

struct thread_info {    /* Used as argument to thread_start() */
    pthread_t thread_id;        /* ID returned by pthread_create() */
    int       thread_num;       /* Application-defined thread # */
    double	  *speeds;
    uint32_t  num_of_speeds;
};

static void *
thread_start(void *arg) {
struct thread_info *tinfo = arg;

double speed = 0.0;
double delta = 0.0;
uint32_t presc = 0;
uint32_t period = 0;

char fname[32];
FILE *f;

	sprintf(fname, "speed_%02d.txt", tinfo->thread_num);
	f = fopen(fname, "w");

	for (uint32_t s = 0; s < tinfo->num_of_speeds; s++) {
		speed = tinfo->speeds[s];
		fprintf(f, "\nFinding timer parameters for speed %f mm/s\n", speed);

		delta = find_minimum_freq(freq(speed), &presc, &period, f);
		fprintf(f, "Speed: %f, need_freq: %f, presc: %d, period: %d, real_freq: %f, error: %f\n",
				speed, freq(speed), presc, period, real_freq(presc, period), delta);

        fprintf(f, "@\t%f\t%d\t%d\n", speed, presc, period);
		fflush(f);
        
        printf("id: %d, speed: %f\n", tinfo->thread_num, speed);
        fflush(stdout);
    }

	fclose(f);
	return 0;
}


int main(int argc, char *argv[]) {

int s;
pthread_attr_t attr;
struct thread_info *tinfo;
void *res;
uint32_t tnum;

	s = pthread_attr_init(&attr);
	pthread_attr_setstacksize(&attr, 10*1024*1024);

	tinfo = calloc(NUM_THREADS, sizeof(struct thread_info));

	for (tnum = 0; tnum < NUM_THREADS; tnum++) {
		double *speeds = calloc(NUM_THREADS, sizeof(double));
		for (uint32_t z = 0; z < 10; z++) {
			speeds[z] = tnum + 0.1*(z+1.0);
		}

		tinfo[tnum].thread_num = tnum;
		tinfo[tnum].speeds = speeds;
		tinfo[tnum].num_of_speeds = 10;
		pthread_create(&tinfo[tnum].thread_id, &attr, &thread_start, &tinfo[tnum]);
	}

	s = pthread_attr_destroy(&attr);

	for (tnum = 0; tnum < NUM_THREADS; tnum++) {
		s = pthread_join(tinfo[tnum].thread_id, &res);
		free(res);
	}



	free(tinfo);
	return 0;
}
