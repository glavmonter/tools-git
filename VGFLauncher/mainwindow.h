#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QStringList>
#include <QFile>
#include <QProcess>
#include <QTimer>
#include <QSettings>
#include <QDir>
#include <QDebug>

namespace Ui {
class MainWindow;
}

enum ServerStatus {
    ServerStopped,
    ServerRunning
};

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void timeout();
    void saveSettings();

    void EditingSettingsFinished();

    void on_btnStartServer_clicked();
    void on_btnStopServer_clicked();
    void on_btnStartSync_clicked();
    void on_btnStopSync_clicked();

    void on_btnRestartCAN_clicked();

private:
    Ui::MainWindow *ui;
    QTimer *m_pTimer10s;
    QTimer *m_pTimerSettings;

    ServerStatus m_xServerStatus = ServerStopped;

    ServerStatus CheckServerStatus(const QString &pidfile = "/tmp/VGFServer.pid");
    int GetServerPid(const QString &pidfile = "/tmp/VGFServer.pid");

    int StartStopServer(bool start);

    QString m_sSettingFileName = QDir::homePath() + "/.vgfserverrc";
};

#endif // MAINWINDOW_H
