#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->statusBar->showMessage(tr("Server pid: %1").arg(GetServerPid()));

    m_pTimer10s = new QTimer();
    connect(m_pTimer10s, &QTimer::timeout, this, &MainWindow::timeout);
    m_pTimer10s->start(10000);

    m_pTimerSettings = new QTimer();
    connect(m_pTimerSettings, &QTimer::timeout, this, &MainWindow::saveSettings);
    m_pTimerSettings->setInterval(1000);
    m_pTimerSettings->setSingleShot(true);

QSettings settings(m_sSettingFileName, QSettings::IniFormat);
    settings.beginGroup("server");
        ui->editCAN->setText(settings.value("can_interface", "vcan0").toString());
        ui->spinServerPort->setValue(settings.value("port", 9999).toInt());
        ui->editServerAddress->setText(settings.value("address", "127.0.0.1").toString());
    settings.endGroup();

    settings.beginGroup("database");
        ui->editDbAddress->setText(settings.value("address", "127.0.0.1").toString());
        ui->editDbName->setText(settings.value("database_name", "default").toString());
    settings.endGroup();

    saveSettings();

QString ipRange = "(?:[0-1]?[0-9]?[0-9]|2[0-4][0-9]|25[0-5])";
QRegExp ipRegex("^" + ipRange
                    + "\\." + ipRange
                    + "\\." + ipRange
                    + "\\." + ipRange + "$");
    ui->editServerAddress->setValidator(new QRegExpValidator(ipRegex, this));
    ui->editDbAddress->setValidator(new QRegExpValidator(ipRegex, this));

    connect(ui->editCAN, SIGNAL(editingFinished()), this, SLOT(EditingSettingsFinished()));
    connect(ui->editCAN, SIGNAL(returnPressed()), this, SLOT(EditingSettingsFinished()));
    connect(ui->editCAN, SIGNAL(textChanged(QString)), this, SLOT(EditingSettingsFinished()));
    connect(ui->editCAN, SIGNAL(textEdited(QString)), this, SLOT(EditingSettingsFinished()));

    connect(ui->editServerAddress, SIGNAL(editingFinished()), this, SLOT(EditingSettingsFinished()));
    connect(ui->editServerAddress, SIGNAL(returnPressed()), this, SLOT(EditingSettingsFinished()));
    connect(ui->editServerAddress, SIGNAL(textChanged(QString)), this, SLOT(EditingSettingsFinished()));
    connect(ui->editServerAddress, SIGNAL(textEdited(QString)), this, SLOT(EditingSettingsFinished()));

    connect(ui->spinServerPort, SIGNAL(valueChanged(int)), this, SLOT(EditingSettingsFinished()));

    connect(ui->editDbAddress, SIGNAL(editingFinished()), this, SLOT(EditingSettingsFinished()));
    connect(ui->editDbAddress, SIGNAL(returnPressed()), this, SLOT(EditingSettingsFinished()));
    connect(ui->editDbAddress, SIGNAL(textChanged(QString)), this, SLOT(EditingSettingsFinished()));
    connect(ui->editDbAddress, SIGNAL(textEdited(QString)), this, SLOT(EditingSettingsFinished()));

    connect(ui->editDbName, SIGNAL(editingFinished()), this, SLOT(EditingSettingsFinished()));
    connect(ui->editDbName, SIGNAL(returnPressed()), this, SLOT(EditingSettingsFinished()));
    connect(ui->editDbName, SIGNAL(textChanged(QString)), this, SLOT(EditingSettingsFinished()));
    connect(ui->editDbName, SIGNAL(textEdited(QString)), this, SLOT(EditingSettingsFinished()));
}

MainWindow::~MainWindow()
{
    saveSettings();
    delete ui;
}


void MainWindow::saveSettings()
{
QSettings settings(m_sSettingFileName, QSettings::IniFormat);
    settings.beginGroup("server");
        settings.setValue("can_interface", ui->editCAN->text());
        settings.setValue("port", ui->spinServerPort->value());
        settings.setValue("address", ui->editServerAddress->text());
    settings.endGroup();

    settings.beginGroup("database");
        settings.setValue("address", ui->editDbAddress->text());
        settings.setValue("port", 27017);
        settings.setValue("database_name", ui->editDbName->text());
    settings.endGroup();
}

void MainWindow::EditingSettingsFinished()
{
    m_pTimerSettings->start(1000);
}

void MainWindow::timeout()
{
    ui->statusBar->showMessage(tr("Server pid: %1").arg(GetServerPid()));
}

ServerStatus MainWindow::CheckServerStatus(const QString &pidfile)
{
    if (GetServerPid(pidfile) > 0)
        return ServerRunning;
    else
        return ServerStopped;
}


int MainWindow::GetServerPid(const QString &pidfile)
{
    QFile f(pidfile);
    if (!f.open(QIODevice::ReadOnly | QIODevice::Text))
        return -1;

    QByteArray arr = f.readAll();
    f.close();

bool ok;
int pid = arr.toInt(&ok, 10);

    if (ok) {
        /* Шлем серверу SIGHUP */
        QProcess kill;
        QStringList args;

        args << "-HUP" << QString("%1").arg(pid);
        kill.start("kill", args);
        kill.waitForFinished(5000);
        QByteArray err = kill.readAllStandardError();
        if (err.size() != 0)
            return -1;
        return pid;
    }
    return -1;
}


int MainWindow::StartStopServer(bool start)
{
    if (start) {
        QProcess serv;
        QStringList args;
        args << "-d";
        serv.start("VGFServer.py", args);
        serv.waitForFinished(5000);
    } else {
        int pid = GetServerPid();
        if (pid < 0)
            return -1;

        QProcess kill;
        QStringList args;
        args << "-INT" << QString("%1").arg(pid);
        kill.start("kill", args);
        kill.waitForFinished(5000);
    }
    return -1;
}

void MainWindow::on_btnStartServer_clicked()
{
    StartStopServer(true);
    timeout();
}

void MainWindow::on_btnStopServer_clicked()
{
    StartStopServer(false);
    QTimer::singleShot(2000, this, SLOT(timeout()));
}

void MainWindow::on_btnStartSync_clicked()
{
int pid = GetServerPid();
    if (pid < 0)
        return;

QProcess kill;
QStringList args;
    args << "-USR1" << QString("%1").arg(pid);
    kill.start("kill", args);
    kill.waitForFinished(5000);
}

void MainWindow::on_btnStopSync_clicked()
{
int pid = GetServerPid();
    if (pid < 0)
        return;

QProcess kill;
QStringList args;
    args << "-USR2" << QString("%1").arg(pid);
    kill.start("kill", args);
    kill.waitForFinished(5000);
}

void MainWindow::on_btnRestartCAN_clicked()
{
QProcess restart;
QStringList args;
    args << "start_can_500000.sh";
    restart.start("sudo", args);
    restart.waitForFinished(5000);
}
