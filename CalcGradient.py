# -*- coding: utf-8 -*-
__author__ = 'Vladimir Meshkov <glavmonter@gmail.com>'


TEMPERATURE_HIGH = 1120     # Верхняя температура
TEMPERATURE_LOW = 1080      # Нижняя температура
TEMPERATURE_MELT = 1095     # Температура кристаллизации
GRADIENT = 10               # Градиент температуры, С/см
ZONE_SIZE = 10              # Толщина зоны, мм

NUMBER_REAL_ZONES = 26      # Количество зон
SPEED = 1                   # Скорость, мм/ч
dT = 600                     # Квант времени, с


HEADER = """
set terminal png size 1920, 1080
set grid xtics ytics mxtics mytics
set ylabel 'T, C'
set xlabel 'Pos, mm'
set xrange [0:220]
set yrange [1000:1125]
set ytics 10
set xtics 10
set title '{title}'
#set mxtics 10

set output 'png/data_{out:06d}.png'
plot '{datafile}' using ($1*10):2 w l lw 3 t '', {melting} w l lw 3 t 'T melt'
"""


def func(x, pos):
    """
    :param x: Точка, центра зоны, мм
    :param pos: Точка, где проходит граница кристаллизации, мм
    :return: Температура, в точке x
    """
    return GRADIENT/ZONE_SIZE*(x - pos) + TEMPERATURE_MELT


class Zone(object):
    def __init__(self, number, teq):
        self.number = number
        self.__func = teq

    def get_temperature(self, pos):
        x = self.number*ZONE_SIZE
        t = self.__func(x, pos)
        if t > TEMPERATURE_HIGH:
            ret = TEMPERATURE_HIGH
        else:
            ret = t
        return ret


def print_temperatures(pos, index):
    data_name = 'data_{f:06d}.dat'.format(f=index)
    plt_name = 'plot_{f:06d}.plt'.format(f=index)

    f_data = open('data/' + data_name, 'w')
    for lz in zones:
        f_data.write('{number}\t{temperature:f}\n'.format(number=lz.number, temperature=lz.get_temperature(pos)))
    f_data.close()

    f_plt = open('data/' + plt_name, 'w')

    title = '{title} s, position: {pos} mm'.format(title=index*dT, pos=pos)
    f_plt.write(HEADER.format(out=index, datafile=data_name, melting=TEMPERATURE_MELT, title=title))

    f_plt.close()

zones = []
for z in range(NUMBER_REAL_ZONES):
    zones.append(Zone(z, func))



sss = input('Введите максимальную температуру ({} C): '.format(TEMPERATURE_HIGH))
try:
    TEMPERATURE_HIGH = float(sss)
except ValueError:
    print('Введено неверное значение. Оставляю по-умолчанию {} C'.format(TEMPERATURE_HIGH))


sss = input('Введите минимальную температуру ({} С): '.format(TEMPERATURE_LOW))
try:
    TEMPERATURE_LOW = float(sss)
except ValueError:
    print('Введено неверное значение. Оставляю по-умолчанию {} C'.format(TEMPERATURE_LOW))


sss = input('Введите температуру кристаллизации ({} C): '.format(TEMPERATURE_MELT))
try:
    TEMPERATURE_MELT = float(sss)
except ValueError:
    print('Введено неверное значение. Оставляю по-умолчанию {} C'.format(TEMPERATURE_MELT))



sss = input('Значемине градиента температуры ({} С/см): '.format(GRADIENT))
try:
    GRADIENT = float(sss)
except ValueError:
    print('Введено неверное значение. Оставляю по-умолчанию {} C/см'.format(GRADIENT))

sss = input('Скорость роста ({} мм/ч): '.format(SPEED))
try:
    SPEED = float(sss)
except ValueError:
    print('Введено неверное значение. Оставляю по-умолчанию {} мм/ч'.format(SPEED))


total_time_of_growth = int(NUMBER_REAL_ZONES * ZONE_SIZE / SPEED * 3600)
speed_mm_s = SPEED / 3600


print('Полное время роста: {time} секунд'.format(time=total_time_of_growth))
print('Скорость {sp} мм/с'.format(sp=speed_mm_s))

current_x = 00.0
z = 0

for i in range(total_time_of_growth // dT):
    print_temperatures(current_x, i)
    current_x += speed_mm_s * dT

print(current_x)

"""
Еще и фильмец из этого забабахать
ffmpeg -framerate 20 -i data_%06d.png -c:v libx264 -r 30 -pix_fmt yuv420p out2.mp4
"""
