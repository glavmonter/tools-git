__author__ = 'supervisor'
# -*- coding: utf-8 -*-

import signal
import socket
import select
import sys
from controller_pb2 import Protocol

from daemonize import Daemonize

TEMPERATURE_HIGH = 1120     # Верхняя температура
TEMPERATURE_LOW = 1080      # Нижняя температура
TEMPERATURE_MELT = 1095     # Температура кристаллизации
GRADIENT = 10               # Градиент температуры, С/см
ZONE_SIZE = 10              # Толщина зоны, мм

NUMBER_REAL_ZONES = 26      # Количество зон
SPEED = 1                   # Скорость, мм/ч
dT = 1                      # Квант времени, с

HEADER = """
set terminal png size 1920, 1080
set grid xtics ytics mxtics mytics
set ylabel 'T, C'
set xlabel 'Pos, mm'
set xrange [0:220]
set yrange [1000:1125]
set ytics 10
set xtics 10
set title '{title}'
#set mxtics 10

set output 'png/data_{out:06d}.png'
plot '{datafile}' using ($1*10):2 w l lw 3 t '', {melting} w l lw 3 t 'T melt'
"""


class SigError(Exception):
    def __init__(self, value):
        self.value = value

    def __str__(self):
        if self.value == signal.SIGHUP:
            return repr('SIGHUP')
        elif self.value == signal.SIGUSR1:
            return repr('SIGUSR1')


def sighandler(signum, frame):
    raise SigError(signum)


def func(x, pos):
    """
    :param x: Точка, центра зоны, мм
    :param pos: Точка, где проходит граница кристаллизации, мм
    :return: Температура, в точке x
    """
    return GRADIENT/ZONE_SIZE*(x - pos) + TEMPERATURE_MELT


class Zone(object):
    def __init__(self, number, teq):
        self.number = number
        self.__func = teq

    def get_temperature(self, pos):
        x = self.number*ZONE_SIZE
        t = self.__func(x, pos)
        if t > TEMPERATURE_HIGH:
            ret = TEMPERATURE_HIGH
        else:
            ret = t
        return ret


class TcpClient(object):
    def __init__(self, hostname, port):
        import logging
        logging.basicConfig(level=logging.DEBUG)
        self.logger = logging.getLogger('TcpClient')
        self._socket = None
        self._hostname = hostname
        self._port = port

        self.lt = []
        self.iteration = 0
        self.speed_mm_s = SPEED / 3600
        self.current_x = 0.0
        self.zones = []

        for z in range(NUMBER_REAL_ZONES):
            self.zones.append(Zone(z, func))

    def start(self):
        self._socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self._socket.connect((self._hostname, self._port))
        self._socket.setblocking(0)

        while True:
            try:
                ready = select.select([self._socket], [], [], 2.0)
                if ready[0]:
                    data = self._socket.recv(2048)
                    if len(data) == 0:
                        self.logger.debug('Remote host closed connection')
                        self._socket.close()

                    self.process_in_data(data)

            except SigError as msg:
                if msg.value == signal.SIGUSR1:
                    self.logger.debug('Iter: {}, Pos: {}'.format(self.iteration, self.current_x))
                    self.logger.debug('T: {}\n'.format(self.lt))
                pass

            except:
                self.logger.debug('Problem handling request')
                self._socket.close()
                return

    def process_in_data(self, data):
        proto_in = Protocol()
        proto_in.ParseFromString(data)

        self.print_temperatures(self.current_x, self.iteration)
        self.current_x += self.speed_mm_s * dT
        self.iteration += 1

    def print_temperatures(self, pos, index):
        self.lt.clear()
        for lz in self.zones:
            self.lt.append(lz.get_temperature(pos))


def main():
    signal.signal(signal.SIGUSR1, sighandler)
    signal.signal(signal.SIGHUP, sighandler)

    import logging
    logging.basicConfig(format='%(filename)s[LINE:%(lineno)d]# %(levelname)-8s [%(asctime)s] %(message)s',
                        datefmt='%Y-%m-%d, %H:%M:%S',
                        level=logging.DEBUG,
                        filename='/tmp/GradientSetter.log',
                        filemode='w')

    logger = logging.getLogger('main')
    logger.setLevel(logging.DEBUG)

    client = TcpClient('127.0.0.1', 5003)

    try:
        logger.info('Listening')
        client.start()
    except KeyboardInterrupt:
        logger.exception('Keyboard interrupt')
    except SigError:
        logger.exception('OSError')
    except:
        logger.exception('Unexpected exception,')

    finally:
        logger.info('Shutting down')

    logger.info('All done')
    sys.exit(0)

# TODO Для демона раскоментировать следующее
daemon = Daemonize(app='test_app', pid='/tmp/GradientSetter.pid', action=main)
daemon.start()

if __name__ == '__main__':
    main()
